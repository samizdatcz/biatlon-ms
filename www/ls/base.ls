toDouble = -> if it < 10 then "0#it" else it.toString!

zavody = d3.tsv.parse ig.data.zavody, (row) ->
  row.wc = row.wc == "1"
  row.date = row.race.match /[0-9]{1,2} [A-Z]{3} [0-9]{4}/ .0
  row.race .= replace " " + row.date, ''
  row.race .= replace "Men " "Men's "
  row.race .= replace "Men's " ""
  row.race .= replace "Individual" "vytrvalostní"
  row.race .= replace "Sprint" "sprint"
  row.race .= replace "Mass Start" "hromadný start"
  row.race .= replace "Pursuit" "stíhací"
  row.race .= replace "km stíhací" "stíhací"
  row.race .= replace "12.5 " ""
  row.race .= replace "10 km " ""
  row.race .= replace "15 km " ""
  row.race .= replace "20 km " ""
  row.order = parseInt row.order, 10
  row

disciplinyAssoc = {}
zavodyAssoc = {}
namesAssoc = {}
for zavod in zavody
  disciplinyAssoc[zavod.race] = 1
  seasonInt = (parseInt zavod.season, 10)
  zavodyAssoc[seasonInt] ?= {}
  zavodyAssoc[seasonInt][zavod.order] = zavod
discipliny = for disciplina of disciplinyAssoc
  disciplina


rocniky = [4 to 15]
  .map ((startYear) -> "#{toDouble startYear}#{toDouble startYear + 1}")

xScale = d3.scale.linear!
  ..domain [0 24]
yScale = d3.scale.linear!
  ..domain [1 25]
results = d3.tsv.parse ig.data.results, (row) ->
  row.points = for field in [1 to 25]
    row[field] = parseInt row[field], 10
    if !row[field] or row[field] > 150
      row[field] = null
      continue
    x = xScale field
    y = Math.min do
      1
      yScale row[field]
    row.season = parseInt row.season, 10
    zavod = zavodyAssoc[row.season][field]
    labelX = "#{field} #{zavod.race} #{zavod.place}"
    labelY = row[field]
    {x, y, labelX, labelY, zavod}

  row.season = parseInt row.season, 10
  namesAssoc[row.Name] = 1
  row
names = for name of namesAssoc => name
container = d3.select ig.containers.base
hashParts = window.location.hash.slice 1
  .split "-"
  .map parseInt _, 10

defaultDisciplina = if (hashParts[0] is 0 or hashParts[0]) then hashParts[0] else 0
defaultRocniky = if (hashParts[1] is 0 or hashParts[1]) then hashParts[1] else 7
defaultPerson = if (hashParts[2] is 0 or hashParts[2]) then hashParts[2] else 0

selectors = container.append \div
  ..append \div
    ..attr \class \selector
    ..append \label
      ..attr \for \selector-person
      ..html "Závodník/závodnice"
    ..append \select
      ..attr \id \selector-person
      ..selectAll \option .data names .enter!append \option
        ..attr \value (d, i) -> i
        ..attr \selected (d, i) -> defaultDisciplina is i || void
        ..html -> it
      ..on \change -> drawOne!
  ..append \div
    ..attr \class \selector
    ..append \label
      ..attr \for \selector-rocniky
      ..html "Ročník"
    ..append \select
      ..attr \id \selector-rocniky
      ..selectAll \option .data rocniky .enter!append \option
        ..attr \value (d, i) -> i
        ..attr \selected (d, i) -> defaultRocniky is i || void
        ..html -> "20#{it.substr 0, 2} - #{it.substr 2}"
      ..on \change -> drawOne!
  ..attr \class \selectors
  ..append \div
    ..attr \class \selector
    ..append \label
      ..attr \for \selector-discipliny
      ..html "Disciplína"
    ..append \select
      ..attr \id \selector-discipliny
      ..selectAll \option .data discipliny .enter!append \option
        ..attr \value (d, i) -> i
        ..attr \selected (d, i) -> defaultPerson is i || void
        ..html -> it
      ..on \change -> drawOne!

selectorDiscipliny = selectors.select \#selector-discipliny
selectorRocniky = selectors.select \#selector-rocniky
selectorPerson = selectors.select \#selector-person

chartContainer = container.append \div
months =
  JAN: "1."
  FEB: "2."
  MAR: "3."
  NOV: "11."
  DEC: "12."
toDate = ->
  [day, month, year] = it.split " "
  "#{day}. #{months[month]} #{year.substr 2}"

drawOne = (jmeno, rocnik, disciplina) ->
  selectedPerson = parseInt selectorPerson.node!value, 10
  selectedRocniky = parseInt selectorRocniky.node!value, 10
  selectedDiscipliny = parseInt selectorDiscipliny.node!value, 10
  jmeno = names[selectedPerson]
  rocnik = rocniky[selectedRocniky]
  disciplina = discipliny[selectedDiscipliny]
  window.location.hash = "#selectedPerson-#selectedRocniky-#selectedDiscipliny"
  person = getOne jmeno, rocnik, disciplina
  if not person
    chartContainer.html 'Tento závodník v tomto roce nezávodil.'
    return
  chartContainer.html ''
  chartContainer.append \ul
    ..selectAll \li .data person.points .enter!append \li
      ..classed \same-disciplina -> it.zavod.race is disciplina
      ..classed \is-wc -> it.zavod.wc
      ..append \div
        ..attr \class \meta
        ..append \span
          ..attr \class \disciplina
          ..html -> it.zavod.race
        ..append \span
          ..attr \class \zavod
          ..html -> "#{it.zavod.place} #{toDate it.zavod.date}"
      ..append \div
        ..attr \class \result-area
        ..append \div
          ..attr \class -> "result result-#{it.labelY}"
          ..style \left -> "#{it.y * 100}%"
          ..html (.labelY)


getWCZavod = (disciplina, rocnik) ->
  zavody
    .filter -> it.wc and it.season is rocnik and it.race is disciplina
    .0
    .order

pickWinners = (zavod, rocnik, pohlavi) ->
  rocnikInt = parseInt rocnik, 10
  results
    .filter -> it.season is rocnikInt
    .filter -> it[zavod]
    .filter -> it.gender is pohlavi
    .sort (a, b) -> a[zavod] - b[zavod]
    .slice 0, 6

getOne = (jmeno, rocnik, disciplina) ->
  rocnikInt = parseInt rocnik, 10
  results
    .filter -> it.Name is jmeno
    .filter -> it.season is rocnikInt
    .0

$ "select"
  .chosen!
  .on \change -> drawOne!

drawOne!
